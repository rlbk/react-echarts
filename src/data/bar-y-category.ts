export const options = {
  title: {
    text: "Operations",
  },
  tooltip: {
    trigger: "axis",
    axisPointer: {
      type: "shadow",
    },
  },
  legend: {},
  grid: {
    left: "3%",
    right: "4%",
    bottom: "3%",
    containLabel: true,
  },
  xAxis: {
    type: "value",
    min: 0.0,
    max: 25.0,
    axisLabel: {
      formatter: "{value} %",
    },
  },
  yAxis: {
    type: "category",
    data: [
      "Product",
      "Office of CEO",
      "Marketing",
      "Customer Support",
      "Finance",
      "HR",
      "IT",
      "Sales",
      "Operations",
    ],
  },
  series: [
    {
      name: "Overall",
      type: "bar",
      color: "#5ac4efcc",
      data: [9.37, 11, 11.9, 12.3, 12.5, 12.9, 13.7, 15.3, 16.6],
    },
    {
      name: "High Performer",
      color: "#c00",
      type: "bar",
      data: [8.59, 0, 10.9, 12, 9.27, 20.8, 11.6, 18.1, 16.6],
    },
  ],
};
