const labelRight = {
  position: "right",
};

export const NegativeBarOption = {
  title: {
    text: "",
  },
  tooltip: {
    trigger: "axis",
    axisPointer: {
      type: "shadow",
    },
  },
  grid: {
    top: 80,
    bottom: 30,
  },
  xAxis: {
    type: "value",
    position: "bottom",
    min: -20.0,
    max: 20.0,
    axisLabel: {
      formatter: "{value} pp",
    },
    splitLine: {
      lineStyle: {
        type: "dashed",
      },
    },
  },
  yAxis: {
    type: "category",
    axisLine: { show: false },
    axisLabel: { show: false },
    axisTick: { show: false },
    splitLine: { show: false },
    data: [
      "0.78",
      "11.0",
      "1.08",
      "0.34",
      "3.2",
      "-7.9",
      "2.04",
      "-2.87",
      "0.01",
    ],
  },
  series: [
    {
      name: "PP",
      type: "bar",
      stack: "Total",
      color: "#55a1af",
      label: {
        show: true,
        formatter: "{b} pp",
      },
      data: [
        0.78,
        11.0,
        1.08,
        0.34,
        3.2,
        { value: -7.9, label: labelRight },
        2.04,
        { value: -2.87, label: labelRight },
        0.01,
      ],
    },
  ],
};
