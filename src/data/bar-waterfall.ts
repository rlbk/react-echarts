export const waterFallBarOptions = {
  title: {
    text: "",
  },
  tooltip: {
    trigger: "axis",
    axisPointer: {
      type: "shadow",
    },
    formatter: function (params: any) {
      let tar;
      if (params[1] && params[1].value !== "-") {
        tar = params[1];
      } else {
        tar = params[2];
      }
      return tar && tar.name + "<br/>" + tar.seriesName + " : " + tar.value;
    },
  },
  legend: {
    data: [],
  },
  grid: {
    left: "2%",
    right: "2%",
    bottom: "2%",
    containLabel: true,
  },
  xAxis: {
    type: "category",
    data: (function () {
      let list = [
        "Expantion",
        "Replacement",
        "Involuntary Turnover",
        "Voluntary Turnover",
        "Discrepancies",
        "Net Chages",
      ];

      return list;
    })(),
  },
  yAxis: {
    type: "value",
  },
  series: [
    {
      type: "bar",
      stack: "Total",
      silent: true,
      itemStyle: {
        borderColor: "transparent",
        color: "transparent",
      },
      emphasis: {
        itemStyle: {
          borderColor: "transparent",
          color: "transparent",
        },
      },
      data: [0, 379, 705, 578, 355, 362, 363],
    },
    {
      type: "bar",
      stack: "Total",
      color: "green",
      label: {
        show: true,
        position: "top",
      },
      data: [379, 326, "-", "-", 7, 362],
    },
    {
      type: "bar",
      stack: "Total",
      color: "pink",
      label: {
        show: true,
        position: "bottom",
      },
      data: ["-", "-", 118, 232, "-", "-"],
    },
  ],
};
