import { createBrowserRouter } from "react-router-dom";
import OptionA from "../pages/OptionA";
import OptionB from "../pages/OptionB";
import App from "../App";

const router = createBrowserRouter([
  { path: "/", element: <App /> },
  { path: "/option-a-solution", element: <OptionA /> },
  { path: "/option-b-solution", element: <OptionB /> },
]);

export default router;
