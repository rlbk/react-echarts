import { Link } from "react-router-dom";
import Style from "./App.module.css";

function App() {
  return (
    <div className={Style.container}>
      <div>
        <h1>See Solution</h1>
        <div className={Style.linkContainer}>
          <Link to="/option-a-solution" className={Style.linkButton}>
            Option A
          </Link>
          <Link to="/option-b-solution" className={Style.linkButton}>
            Option B
          </Link>
        </div>
      </div>
    </div>
  );
}

export default App;
