import MainSectionA from "../../components/MainSectionA";
import MainLoyout from "../../layout/MainLoyout";
import AsideSectionA from "../../components/AsideSectionA";

const OptionA = () => {
  return (
    <MainLoyout>
      <MainSectionA />
      <AsideSectionA />
    </MainLoyout>
  );
};

export default OptionA;
