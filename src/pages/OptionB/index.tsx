import AsideSectionB from "../../components/AsideSectionB";
import MainSectionB from "../../components/MainSectionB";
import MainLoyout from "../../layout/MainLoyout";

const OptionB = () => {
  return (
    <MainLoyout>
      <MainSectionB />
      <AsideSectionB />
    </MainLoyout>
  );
};

export default OptionB;
