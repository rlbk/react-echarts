interface MainLayoutPropsI {
  children: React.ReactNode;
}

const MainLoyout = ({ children }: MainLayoutPropsI) => {
  return (
    <div
      style={{
        height: "100vh",
        width: "100vw",
        display: "flex",
        overflow: "hidden",
      }}
    >
      {children}
    </div>
  );
};

export default MainLoyout;
