interface AsideLayoutPropsI {
  children: React.ReactNode;
  title: string;
}

const AsideLayout = ({ children, title }: AsideLayoutPropsI) => {
  return (
    <aside
      style={{
        height: "100%",
        width: " 20%",
        background: "#eee",
        padding: "0.6rem ",
      }}
    >
      <h1 style={{ margin: "0", marginBottom: "1rem" }}>{title}</h1>
      {children}
    </aside>
  );
};

export default AsideLayout;
