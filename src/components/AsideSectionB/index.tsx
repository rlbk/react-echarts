import AsideLayout from "../../layout/AsideLayout";
import TablePoint, { TablePointI } from "../reusable/TablePoint";
import TableRow, { TableRowI } from "../reusable/TableRow";
import Style from "./AsideSectionB.module.css";

const summaryTableValue: TableRowI[] = [
  {
    title: "HeadCount",
    value: "+397",
    color: "#000",
  },
  {
    title: "Starting HeadCount",
    value: "4.85K",
    color: "#000",
  },
  {
    title: "Ending HeadCount",
    value: "5.38K",
    color: "#000",
  },

  {
    title: "Net in",
    value: "720",
    color: "#000",
  },
  {
    title: "Net out",
    value: "348",
    color: "#000",
  },
];

const tablePointValues: TablePointI[] = [
  {
    text: "Scans",
    color: "green",
  },
  {
    text: "Exits",
    color: "red",
  },
  {
    text: "Discrepancies",
    color: "gray",
  },
  {
    text: "Net Changes",
    color: "skyBlue",
  },
];

const AsideSectionB = () => {
  return (
    <AsideLayout title="Net chage">
      <div className={Style.backgroundWhite}>
        {summaryTableValue.map((tableValue, idx) => (
          <TableRow
            width="96%"
            key={idx}
            title={tableValue.title}
            value={tableValue.value}
            color={tableValue.color}
          />
        ))}
      </div>

      <h3 className={Style.textBold}>Legend</h3>
      <p className={Style.textPara}>Movement Summary</p>
      <div className={[Style.backgroundWhite, Style.extraPadding].join(" ")}>
        {tablePointValues.map((tablePoint, idx) => (
          <TablePoint
            key={idx}
            text={tablePoint.text}
            color={tablePoint.color}
          />
        ))}
      </div>
    </AsideLayout>
  );
};

export default AsideSectionB;
