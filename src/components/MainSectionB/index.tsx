import { BsCalendarDate } from "react-icons/bs";
import { IoAdd } from "react-icons/io5";
import Style from "./MainSectionB.module.css";
import FilterButton from "../reusable/FilterButton";
import ReactEcharts from "echarts-for-react";
import { waterFallBarOptions } from "../../data/bar-waterfall";

const MainSectionB = () => {
  return (
    <section className={Style.container}>
      <div className={Style.headingButton}>Employee Movement Breakdown</div>
      <div className={Style.filterContainer}>
        <FilterButton Icon={BsCalendarDate} title="2019" />
        <FilterButton Icon={IoAdd} title="Add filter" />
      </div>
      <div style={{ height: "70vh" }}>
        <ReactEcharts option={waterFallBarOptions} style={{ height: "100%" }} />
      </div>
    </section>
  );
};

export default MainSectionB;
