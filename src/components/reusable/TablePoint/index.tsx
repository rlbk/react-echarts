export interface TablePointI {
  color?: string;
  text: string;
}

const TablePoint = ({ color = "#000", text }: TablePointI) => {
  return (
    <div
      style={{
        color,
        display: "flex",
        alignItems: "center",
        gap: "1rem",
        padding: "0.8rem ",
        width: "100%",
        borderBottom: "2px solid #aaa",
      }}
    >
      <div
        style={{
          height: "8px",
          width: "8px",
          borderRadius: "100%",
          backgroundColor: color,
        }}
      ></div>
      <span>{text}</span>
    </div>
  );
};

export default TablePoint;
