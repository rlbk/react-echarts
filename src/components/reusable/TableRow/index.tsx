export interface TableRowI {
  title: string;
  value: string;
  color?: string;
}

interface ExtraTableRowPropsI {
  width?: string;
  padding?: string;
}

const TableRow = ({
  color = "black",
  title,
  value,
  width = "86%",
  padding = "0 0.5rem",
}: TableRowI & ExtraTableRowPropsI) => {
  return (
    <div
      style={{
        color,
        display: "flex",
        width,
        justifyContent: "space-between",
        alignItems: "center",
        padding,
        margin: "auto",
        borderTop: "2px solid #ccc",
        fontSize: "80%",
      }}
    >
      <p>{title}</p>
      <p>{value}</p>
    </div>
  );
};

export default TableRow;
