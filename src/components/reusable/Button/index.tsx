interface ButtonPropsI {
  textAlign?: "center" | "left" | "right";
  width?: string;
  padding?: string;
  textSize?: string;
  margin?: string;
  children: React.ReactNode;
}

const Button = ({
  textAlign = "left",
  width = "100%",
  padding = "0.8rem",
  textSize = "18px",
  margin = "0.5rem 0.3rem",
  children,
}: ButtonPropsI) => {
  return (
    <button
      style={{
        backgroundColor: "transparent",
        border: "1px solid #ccc",
        textAlign,
        width,
        padding,
        fontWeight: "700",
        fontSize: textSize,
        borderRadius: "4px",
        margin,
        cursor: "pointer",
      }}
      className="button"
    >
      {children}
    </button>
  );
};

export default Button;
