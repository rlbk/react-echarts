import { IconType } from "react-icons";
import Style from "./FilterButton.module.css";

interface FilterButtonPropsI {
  Icon: IconType;
  title: string;
}

const FilterButton = ({ Icon, title }: FilterButtonPropsI) => {
  return (
    <div className={[Style.filterButtonContainer, Style.container].join(" ")}>
      <div className={Style.filterButton}>
        <Icon size={16} />
        <span className={Style.filterButtonTitle}>{title}</span>
      </div>
    </div>
  );
};

export default FilterButton;
