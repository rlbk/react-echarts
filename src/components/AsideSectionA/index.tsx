import AsideLayout from "../../layout/AsideLayout";
import Button from "../reusable/Button";
import TableRow, { TableRowI } from "../reusable/TableRow";
import Style from "./AsideSectionA.module.css";

const summaryTableValue: TableRowI[] = [
  {
    title: "Overall",
    value: "14.0%",
    color: "#5ac4ef",
  },
  {
    title: "Resignation Count",
    value: "639",
    color: "#555",
  },
  {
    title: "Averate HeadCount",
    value: "4.58 K",
    color: "#555",
  },
  {
    title: "High Performer",
    value: "14.1%",
    color: "#c00",
  },
  {
    title: "Resignaton Count",
    value: "152",
    color: "#555",
  },
  {
    title: "Averate HeadCount",
    value: "108 K",
    color: "#555",
  },
  {
    title: "Difference",
    value: "-0.16 pp",
    color: "#0a0",
  },
];

const AsideSectionA = () => {
  return (
    <AsideLayout title="Summary">
      <h3>Apr 2018 - Mar 2019</h3>
      <div className={Style.backgroundWhite}>
        {summaryTableValue.map((tableValue, idx) => (
          <TableRow
            key={idx}
            title={tableValue.title}
            value={tableValue.value}
            color={tableValue.color}
          />
        ))}
      </div>
      <Button textAlign="center" width="98%" margin="1rem 0.3rem">
        View Details
      </Button>
      <h3 className={Style.textBold}>Legend</h3>
      <div className={[Style.backgroundWhite, Style.extraPadding].join(" ")}>
        <p>
          Not all data are shown in this chart. To Show these values, go to{" "}
          <br />
          <span className={Style.textUpperSky}>CHAT SETTINGS</span>
        </p>
      </div>
    </AsideLayout>
  );
};

export default AsideSectionA;
