import { IoMdArrowDropdownCircle } from "react-icons/io";
import { BsCalendarDate } from "react-icons/bs";
import { IoAdd } from "react-icons/io5";
import Style from "./MainSectionA.module.css";
import FilterButton from "../reusable/FilterButton";
import ReactEcharts from "echarts-for-react";
import { options } from "../../data/bar-y-category";
import { NegativeBarOption } from "../../data/bar-negative";

const MainSectionA = () => {
  return (
    <section className={Style.container}>
      <div className={Style.titleDescContainer}>
        <div className={Style.headingContainer}>
          <h1 className={Style.headingH1}>
            Comparision of high performer resignation rates to the overall
            regisnation rate
          </h1>
          <IoMdArrowDropdownCircle size={32} />
        </div>
        <p>Do high perfomers resign more often than others?</p>
      </div>
      <div className={Style.filterContainer}>
        <FilterButton Icon={BsCalendarDate} title="Mar 2018" />
        <FilterButton Icon={IoAdd} title="Add a filter" />
      </div>
      <div className={Style.chartContainer}>
        <div className={Style.chart}>
          <ReactEcharts option={options} style={{ height: "100%" }} />
        </div>
        <div className={Style.chart}>
          <ReactEcharts option={NegativeBarOption} style={{ height: "100%" }} />
        </div>
      </div>
    </section>
  );
};

export default MainSectionA;
